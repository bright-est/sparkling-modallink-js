module.exports = function (grunt) {

    grunt.initConfig({

		// watch: {
		// 	files: ['src/sass/**/*.scss'],
		// 	tasks: ['default']
        // },
        clean: ["deploy"],
        copy: {
            main: {
                expand: true,
                cwd: 'src/',
                src: '**',
                dest: 'deploy/',
            },
            packagejson: {
                src: 'package.json',
                dest: 'deploy/package.json',
            }
        },
        uglify: {
            options: {
                mangle: true,
                compress: false,
                sourceMap: true
            },
            js: {
                files: {
                    'deploy/sparkling-modal-link.min.js': ['deploy/sparkling-modal-link.js']
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                sourceMap: true
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'deploy',
                    src: ['*.css', '!*.min.css'],
                    dest: "deploy",
                    ext: '.min.css'
                }]
            }
        },
        postcss: {
            options: {
                map: false,
                processors: [
                    require("postcss-cssnext")({ browsers: ['last 5 versions'] }),
                ]
            },
            dist: {
                src: ['deploy/*.css', 'deploy/!*.min.css'],
            }
        },
        run: {
            options: {
                cwd: "deploy"
            },
            npm: {
                exec: "npm pack && npm publish"
            }
        }
    });

    grunt.registerTask("npmrc", function () {
        grunt.file.write(process.env.USERPROFILE + "/.npmrc", "@brightspark:registry=https://www.myget.org/F/brightspark/npm/\r\n\
//www.myget.org/F/brightspark/npm/:_authToken=b101a971-03c8-4bef-9e04-4e39625faf02\r\n\
//www.myget.org/F/brightspark/npm/:always-auth=true")
    });

    // grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-run');
    grunt.registerTask('deploy', ['default', 'npmrc', 'run:npm']);
    grunt.registerTask('default', ['clean', 'copy', 'uglify', 'postcss', 'cssmin']);
};