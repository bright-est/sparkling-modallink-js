# Install

    npm install @brightspark/sparkling-modal-link-js

If you haven't set brightspark registry yet, you should run 

    npm config set @brightspark:registry="https://www.myget.org/F/brightspark/auth/0ba9cd9a-62b4-4ba4-9c06-f12b76fde17b/npm/"


# sparkling.modalLink

## Basic usage   


```
#!javascript

sparkling.modalLink.bind(document.querySelector("#my-link"));
sparkling.modalLink.bind(document.querySelectorAll(".modal-link"));
sparkling.modalLink.bind(".modal-link");  
```


## Open link with additional data


```
#!javascript

sparkling.modalLink.bind(".modal-link-with-data", {
    data: {
        foo: "bar"
    }
});
```

Data can be also a callback

```
#!javascript

sparkling.modalLink.bind(".modal-link-with-data", {
    data: function () {
        return serializeSomeForm();
    }
});
```


## Open link with additional data with POST


```
#!javascript

sparkling.modalLink.bind(".modal-link-posting-data", {
    method: "POST",
    data: {
        foo: "bar"
    }
});
```

## Open in script


```
#!javascript

var el_btnClickMe = document.querySelector(".btn-click-me");
el_btnClickMe.addEventListener("click",
    function() {
        sparkling.modalLink.openUrl("Page2.html", {
            title: "Global open"
        })
    });
```


## Open unbound link in modal


```
#!javascript

sparkling.modalLink.openUrl(url, settings);
```

or

```
#!javascript

sparkling.modalLink.openLink(el, settings);
```


## Close modal window inside modal


```
#!javascript

(function (modalLink) {
    var el_close = document.querySelector(".close-modal");
    el_close.addEventListener("click", function () {
        modalLink.close();
    });
})(window.parent.sparkling.modalLink);
```


## Methods

### open
TBD

### close
TBD

## Events

### modallink.close
TBD

## Options

### title

Sets modal window title.
It can be set through attribute **data-ml-title** or **title**. If not any of these attributes is set, value will be selected form link text.

        <a href="target-url" data-ml-title="Edit content">link</a>
        <a href="target-url" title="Edit content">link</a>
        <a href="target-url">Edit content</a>
        
or with options when initializin modal link

        $link.modalLink({
            title: "Edit content"
        });
        
Value is always fetched from options first. 

So the priority list of defined title values looks like this:
* options value
* data-ml-title attribute value
* title attribute value
* link text

**NOTE:** Title do not have default value.

### showTitle

Sets if modal window titlebar is shown.
Can be set through **data-ml-show-title** attribute.

        <a href="target-url" data-ml-show-title="false">link</a>
        
or via options

        $link.modalLink({
            showTitle: false
        });

If none of above is set, value will be fetched from defaults. [See defaults](README.md#Defaults).

Priority list of showTitle value looks like this:
* options
* attribute
* default

### showClose

Sets if close button is shown on modal window titlebar.
Can be set through **data-ml-show-close** attribute.

        <a href="target-url" data-ml-show-title="false">link</a>
        
or via options

        $link.modalLink({
            showClose: false
        });
        
If none of above is set, value will be fetched from defaults. [See defaults](README.md#Defaults).

Priority list of showClose value looks like this:
* options
* attribute
* default

### width 

Sets modal window content width.
It can be set through **data-ml-width** attribute

        <a href="target-url" data-ml-width="400">link</a>
        
or with options when initializin modal link

        $link.modalLink({
            width: 400
        });
        
Value is always fetched from options first. 
If none of above is set, value will be fetched from defaults. [See defaults](README.md#Defaults).

So the priority list looks like this:
* option
* attribute
* default

### height

Sets modal window content height.
It can be set through **data-ml-height** attribute

        <a href="target-url" data-ml-height="400">link</a>
        
or with options when initializin modal link

        $link.modalLink({
            height: 400
        });
        
Value is always fetched from options first. 
If none of above is set, value will be fetched from defaults. [See defaults](README.md#Defaults)

So the priority list looks like this:
* option
* attribute
* default

### method

* REF
* CLONE
* GET
* POST

### data
### overlayOpacity
### $form

### disableScroll

### onHideScroll

hide scroll callback

##Styling

* .sparkling-modal-container
* .sparkling-modal-overlay
* .sparkling-modal-frame
* .sparkling-modal-title
* .sparkling-modal-title span
* .sparkling-modal-close
* .sparkling-modal-content

## Defaults

TBD

## Templates

TBD