interface IModalLinkSettings {
    url?: string;
    title?: string;
    showtitle?: boolean;
    showClose?: boolean;
    width?: number;
    height?: number;
    method?: "GET" | "POST" | "REF" | "CLONE",
    target?: "iframe" | "inline" | "_blank",
    data?: () => any | object | any[] | string,
    disableScroll?: boolean,
    autoSizeX?: boolean,
    maxWidth?: number,
    autoSizeY?: boolean,
    maxHeight?: boolean,
    centerX?: boolean,
    centerY?: boolean,
    templates?: {
        container?: string | DocumentFragment,
        overlay?: string | DocumentFragment,
        frame?: string | DocumentFragment,
        title?: string | DocumentFragment,
        close?: string | DocumentFragment,
        content?: string | DocumentFragment,
    },
    titleSelector?: string,
    onClose?: (data?: any) => void,
    alwaysOpenOnRootWindow?: boolean,
    setFrameSizeExplicitly?: boolean
}

declare class modalLink {

    static overrideDefaults(newDefaults: IModalLinkSettings): void;
    static bind(elements: NodeList | Element | Element[] | string, settings?: IModalLinkSettings): void;

    static open(url: string, settings?: IModalLinkSettings): modalLink;
    static openLink(el: HTMLAnchorElement, settings?: IModalLinkSettings): modalLink;
    static close(data?: any): void;

    static ajaxPostForm: (url: string, callback: (res: string) => void) => void;
    static ajaxGetUrl: (url: string, callback: (res: string) => void) => void;

    constructor(el: Element, settings?: IModalLinkSettings);

    close(data?: any): void;
    onClose(handler: (data?: any) => void): modalLink;

    open(overrides?: IModalLinkSettings): void;
}