if (typeof sparkling === "undefined") {
    sparkling = {};
}

(function (ns) {

    var _method = {
        GET: "GET",
        POST: "POST",
        REF: "REF",
        CLONE: "CLONE"
    };

    var _target = {
        BLANK: "_blank",
        IFRAME: "iframe",
        INLINE: "inline"
    };

    var _events = {
        open: "open",
        close: "close",
        hideScroll: "hideScroll",
        showScroll: "showScroll",
        hideContentScroll: "hideContentScroll",
        showContentScroll: "showContentScroll"
    };

    var _defaults = {
        url: undefined,
        title: undefined,
        showTitle: true,
        showClose: true,
        width: 900,
        height: 600,
        method: _method.GET, // GET, POST, REF, CLONE
        target: _target.IFRAME, // iframe (GET, POST, CLONE) | inline (REF, CLONE)
        data: undefined,
        disableScroll: true,
        autoSizeX: false,
        maxWidth: undefined,
        autoSizeY: false,
        maxHeight: undefined,
        centerX: true,
        centerY: true,
        templates: {
            container: "<div class=\"sparkling-modal-container\"></div>",
            overlay: "<div class=\"sparkling-modal-overlay\"></div>",
            frame: "<div class=\"sparkling-modal-frame\"></div>",
            title: "<div class=\"sparkling-modal-title\"></div>",
            close: "<div class=\"sparkling-modal-close\"><div class='i-close'><div class='i-close-h'></div><div class='i-close-v'></div></div></div>",
            content: "<div class=\"sparkling-modal-content\"></div>"
        },
        titleSelector: undefined,
        onClose: undefined,
        alwaysOpenOnRootWindow: true,
        setFrameSizeExplicitly: true
    };

    var _registry = {};

    var _global = {};
    ns.Observable.call(_global);

    _global.onHideScroll = _global.createPublishSubscribe(_events.hideScroll);
    _global.onShowScroll = _global.createPublishSubscribe(_events.showScroll);
    _global.onHideContentScroll = _global.createPublishSubscribe(_events.hideContentScroll);
    _global.onShowContentScroll = _global.createPublishSubscribe(_events.showContentScroll);
    _global.stack = [];

    _global.close = function (data) {
        var ml = _global.stack.pop();
        if (ml) {
            ml.close(data);
        }
    };

    _global.open = function (url, settings) {
        var el = document.createElement("a");
        settings = settings || {};
        settings.url = url || settings.url;

        var ml = new modalLink(el, settings);
        ml.open();

        return ml;
    };

    _global.openLink = function (el, settings) {
        var ml = new modalLink(el, settings);
        ml.open();
        return ml;
    };

    _global.bind = function (elements, settings) {

        if (typeof elements === "string") {
            var selector = elements;
            elements = document.querySelectorAll(selector);
        }
        else if (elements instanceof Element) {
            elements = [elements];
        }
        else if (elements instanceof NodeList) {
            // OK
        }
        else {
            console.error("Invalid argument", elements)
        }

        for (var i = 0, l = elements.length; i < l; i++) {
            new modalLink(elements[i], settings);
        }
    };

    _global.overrideDefaults = function (newDefaults) {
        overrideDefaults(newDefaults, _defaults);
    };

    var _mlCount = 0;

    var modalLink = function (el, settings) {

        var isAlreadyBound = typeof el.__mlId !== "undefined" && typeof _registry[el.__mlId] !== "undefined";
        if (isAlreadyBound) {
            return _registry[el.__mlId];
        }

        var _id = "ml_" + _mlCount++;
        el.__mlId = _id;

        _registry[_id] = this;

        var _this = this;
        var _settings = _helper.clone(settings) || {};
        // var _el = el;
        var _el_overlay;
        var _el_frame;
        var _el_container;
        var _el_title;
        var _el_content;

        var _isOpen = false;
        var _isScrollVisible = false;

        var _contentHeightCheckerInterval;

        ns.Observable.call(_this);

        this.onClose = function (handler) {
            _this.subscribe(_events.close, handler);
            return _this;
        };

        this.close = function (data) {
            if (!_isOpen) {
                return;
            }

            _isOpen = false;

            document.removeEventListener("keyup", escHandler);

            _this.publish(_events.close, data);

            // todo - use fadeTo if transitions are not supported
            // $overlay.fadeTo("fast", 0);
            _el_overlay.classList.remove("on");

            var transitionEnd = function () {
                if (_el_container.parentNode) {
                    _el_container.parentNode.removeChild(_el_container);
                }
                showBodyScroll();
            };

            _el_frame.addEventListener("transitionend", transitionEnd);
            _el_frame.addEventListener("webkitTransitionEnd", transitionEnd);
            _el_frame.addEventListener("oTransitionEnd", transitionEnd);
            _el_frame.addEventListener("otransitionend", transitionEnd);
            _el_frame.addEventListener("msTransitionEnd", transitionEnd);

            _el_frame.classList.remove("visible");

            clearInterval(_contentHeightCheckerInterval);

            // $content.fadeTo("fast", 0, function () {
            //     $container.remove();
            //     showBodyScroll(cb);

            //     if (typeof cb == "function") {
            //         cb();
            //     }
            // });
        };

        this.open = function (overrides) {

            _isOpen = true;
            _isScrollVisible = false;

            var inlineSettings = {
                url: el.getAttribute("href"),
                title: el.getAttribute("data-ml-title") || el.getAttribute("title") || el.innerHTML,
                showTitle: _helper.getAttrAsBoolean(el, "data-ml-show-title"),
                showClose: _helper.getAttrAsBoolean(el, "data-ml-show-close"),
                disableScroll: _helper.getAttrAsBoolean(el, "data-ml-disable-scroll"),
                width: _helper.getAttrAsInt(el, "data-ml-width"),
                height: _helper.getAttrAsInt(el, "data-ml-height"),
                target: el.getAttribute("data-ml-target"),
            }

            var currentSettings = buildSettings(overrides, _settings, inlineSettings);

            if (typeof currentSettings.onClose === "function") {
                var f = function (data) {
                    currentSettings.onClose.call(_this, data);
                    _this.unsubscribe(_events.close, f);
                }

                _this.onClose(f);
            }

            if (currentSettings.target === _target.BLANK) {

                if (currentSettings.method === _method.POST) {
                    openPost(currentSettings);
                } else {
                    window.open(currentSettings.url);
                }

                return;
            }

            var iframe = createModal(currentSettings);

            switch (currentSettings.method) {
                case _method.REF:
                    openRef(currentSettings, iframe);
                    break;
                case _method.CLONE:
                    openClone(currentSettings, iframe);
                    break;
                case _method.POST:
                    openPost(currentSettings, iframe);
                    break;
                default:
                    openGet(currentSettings, iframe);
                    break;
            }

            autoSizeIfSet(currentSettings, iframe);
            centerIfSet(currentSettings);

            _helper.css(_el_frame, {
                "max-width": currentSettings.width + "px",
                "max-height": currentSettings.height + "px"
            });

            _el_overlay.classList.add("on");
            _el_frame.classList.add("visible");

            if (currentSettings.disableScroll) {
                hideBodyScroll();
            }

            _this.publish(_events.open);

            document.addEventListener("keyup", escHandler);
        };

        function escHandler(e) {
            if (e.keyCode == 27) {
                _this.close();
            }
        }

        function autoSizeIfSet(currentSettings, iframe) {
            if (!currentSettings.autoSizeY && ! currentSettings.autoSizeX) {
                return;
            }

            _contentHeightCheckerInterval = setInterval(function () {
                if (!iframe.contentDocument || !iframe.contentDocument.documentElement) {
                    return;
                }

                if (currentSettings.autoSizeY) {
                    // Calculate max content height to fit the browser window
                    var innerHeight = iframe.contentDocument.documentElement.offsetHeight;
                    var innerContentPadding = _el_content.offsetHeight - iframe.offsetHeight;
                    var titleHeight = _el_title ? _el_title.offsetHeight : 0;

                    var offsetTop = Math.max(0, _el_frame.offsetTop);
                    var maxHeight = Math.max(0, window.innerHeight - offsetTop - titleHeight - innerContentPadding);
                    if (currentSettings.maxHeight) {
                        maxHeight = Math.min(currentSettings.maxHeight, maxHeight);
                    }

                    if (!_isScrollVisible && maxHeight < innerHeight) {
                        _global.publish(_events.showContentScroll, _el_container);
                        _isScrollVisible = true;
                    } else if (_isScrollVisible && maxHeight >= innerHeight) {
                        _global.publish(_events.hideContentScroll, _el_container);
                        _isScrollVisible = false;
                    }

                    var height = Math.min(maxHeight, innerHeight);

                    _helper.css(iframe, {
                        height: height + "px"
                    });
                    centerYIfSet(currentSettings);
                }
                if (currentSettings.autoSizeX) {
                    var width = iframe.contentDocument.documentElement.offsetWidth;
                    _helper.css(iframe, {
                        width: width + "px"
                    });
                    centerXIfSet(currentSettings);
                }
            }, 100);
        }

        function centerIfSet(currentSettings) {
            centerXIfSet(currentSettings);
            centerYIfSet(currentSettings);
        }

        function centerXIfSet(currentSettings) {
            if (!currentSettings.centerX) {
                return;
            }

            _helper.css(_el_frame, {
                "margin-left": (-_el_frame.offsetWidth / 2) + "px",
                "left": "50%"
            });
        }

        function centerYIfSet(currentSettings) {
            if (!currentSettings.centerY) {
                return;
            }

            _helper.css(_el_frame, {
                "margin-top": (-_el_frame.offsetHeight / 2) + "px",
                "top": "50%"
            });
        }

        function buildSettings(overrides, settings, inlineSettings, defaults) {

            overrides = overrides || {};
            settings = settings || {};
            inlineSettings = inlineSettings || {};
            defaults = defaults || _defaults;

            var res = {};
            for (var i in defaults) {
                if (defaults.hasOwnProperty(i)) {

                    if (typeof defaults[i] === "object") {
                        res[i] = buildSettings(overrides[i], settings[i], inlineSettings[i], defaults[i]);
                    }
                    else {
                        res[i] = _helper.coalesce(overrides[i], settings[i], inlineSettings[i], defaults[i]);
                    }
                }
            }

            if (res.method !== _method.CLONE && res.url && res.url.length > 0 && res.url[0] === "#") {
                res.method = _method.REF;
            }

            if ((res.method === _method.GET || res.method === _method.POST) && settings.target != _target.BLANK) {
                res.url = _helper.addUrlParam(res.url, "__inmodal", "true");
            }

            // exec function if data was passed via function
            if (typeof res.data === "function") {
                res.data = res.data();
            }

            return res;
        }

        function renderContainer(currentSettings) {

            var rootWindow = window;

            if (currentSettings.alwaysOpenOnRootWindow) {
                while (rootWindow.parent !== rootWindow) {
                    rootWindow = rootWindow.parent;
                }
            }

            rootWindow.modalLink.stack.push(_this);

            var body = rootWindow.document.body;

            _el_container = _helper.parseHtml(currentSettings.templates.container);
            body.appendChild(_el_container);

            _el_overlay = _helper.parseHtml(currentSettings.templates.overlay);
            _el_container.appendChild(_el_overlay);

            _el_frame = _helper.parseHtml(currentSettings.templates.frame);
            _el_container.appendChild(_el_frame);

            if (currentSettings.showTitle) {

                _el_title = _helper.parseHtml(currentSettings.templates.title);
                _el_frame.appendChild(_el_title);

                var el_titleText;
                if (currentSettings.titleSelector) {
                    el_titleText = _el_title.querySelector(currentSettings.titleSelector);
                } else {
                    el_titleText = _helper.createElement("span");
                    _el_title.appendChild(el_titleText);
                }

                el_titleText.innerHTML = currentSettings.title;

                if (currentSettings.showClose) {
                    var el_closeButton = _helper.parseHtml(currentSettings.templates.close);
                    _el_title.appendChild(el_closeButton);

                    el_closeButton.addEventListener("click", _this.close);
                }
            }

            _el_content = _helper.parseHtml(currentSettings.templates.content);
            _el_frame.appendChild(_el_content);

            return _el_content;
        }

        function renderContent(currentSettings) {

            if (currentSettings.method === _method.REF || currentSettings.target === _target.INLINE) {

                return _helper.createElement("div", {
                    style: "overflow: auto"
                });
            }

            return _helper.createElement("iframe", {
                "frameborder": "0",
                "id": "modal-frame",
                "name": "modal-frame"
            });
        }

        function createModal(currentSettings) {

            var el_container = renderContainer(currentSettings);
            var iframe = renderContent(currentSettings);

            if (currentSettings.setFrameSizeExplicitly) {
                _helper.css(iframe, {
                    width: currentSettings.width + "px",
                    height: currentSettings.height + "px"
                });
            }

            el_container.appendChild(iframe);

            return iframe;
        }

        function openGet(currentSettings, iframe) {
            if (typeof currentSettings.data === "object") {
                for (var i in currentSettings.data) {
                    if (currentSettings.data.hasOwnProperty(i)) {
                        currentSettings.url = _helper.addUrlParam(currentSettings.url, i, currentSettings.data[i]);
                    }
                }
            }
            else if (typeof currentSettings.data === "string") {
                currentSettings.url = _helper.appendUrl(currentSettings.url, currentSettings.data);
            }

            if (currentSettings.target === _target.INLINE) {
                modalLink.ajaxGet(currentSettings.url, function (res) {
                    iframe.innerHTML = res;
                });
                return;
            }

            iframe.setAttribute("src", currentSettings.url);
        }

        function openPost(currentSettings, iframe) {

            if (currentSettings.method === _target.INLINE) {
                modalLink.ajaxPost(currentSettings.url, currentSettings.data, function (res) {
                    iframe.innerHTML = res;
                });
                return;
            }

            var form = _helper.createElement("form", {
                "action": currentSettings.url,
                "method": _method.POST,
                "target": iframe ? iframe.id : _target.BLANK,
            });
            form.style.display = "none";

            var el_sparklingModalInit = _helper.createElement("input", {
                "type": "hidden",
                "name": "__sparklingModalInit",
                "value": "1"
            });
            form.appendChild(el_sparklingModalInit);

            var data = currentSettings.data;
            if (data !== 'undefined') {

                if (typeof data === "function") {
                    data = data();
                }

                if (Array.isArray(data)) {
                    for (var i in data) {
                        form.appendChild(
                            _helper.createElement("input", {
                                type: "hidden",
                                name: data[i].name,
                                value: data[i].value
                            })
                        );
                    }
                }
                else {
                    for (var i in data) {
                        form.appendChild(
                            _helper.createElement("input", {
                                type: "hidden",
                                name: i,
                                value: data[i]
                            })
                        );
                    }
                }
            }

            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }

        function openClone(currentSettings, iframe) {
            var sourceElement = document.querySelector(currentSettings.url);

            var iFrameDoc = iframe.contentDocument || iframe.contentWindow.document;
            if (sourceElement) {
                iFrameDoc.write(sourceElement.innerHTML);
            }
            iFrameDoc.close();
        }

        function openRef(currentSettings, iframe) {
            var targetElement = document.querySelector(currentSettings.url);

            var temporaryContainer = document.createElement("div");
            temporaryContainer.id = "ref_" + new Date().getTime();

            var parentNode = targetElement.parentNode;
            parentNode.insertBefore(temporaryContainer, targetElement.nextSibling);

            iframe.appendChild(targetElement);

            var handler = function () {
                parentNode.insertBefore(targetElement, temporaryContainer);
                parentNode.removeChild(temporaryContainer);
                _this.unsubscribe(_events.close, handler);
            };

            _this.subscribe(_events.close, handler);
        }

        /**
         * 
         */
        function hideBodyScroll() {
            var w = document.body.offsetWidth;
            _helper.css(document.body, {
                overflow: "hidden"
            });

            var w2 = document.body.offsetWidth;
            _helper.css(document.body, {
                width: w + "px"
            });

            var scrollbarWidth = w2 - w;
            _global.publish(_events.hideScroll, scrollbarWidth);
        }

        /**
         * 
         */
        function showBodyScroll() {
            var w = document.body.offsetWidth;
            _helper.css(document.body, {
                width: '',
                overflow: ''
            });
            var w2 = document.body.offsetWidth;

            var scrollbarWidth = w - w2;
            _global.publish(_events.showScroll, scrollbarWidth);
        }

        // init

        el.addEventListener("click",
            function (e) {
                e.preventDefault();

                var temporarySettings;
                if (e.ctrlKey) {
                    temporarySettings = {
                        target: _target.BLANK
                    };
                }

                _this.open(temporarySettings);
            });
    };

    function overrideDefaults(newDefaults, defaults) {

        for (var i in newDefaults) {
            if (newDefaults.hasOwnProperty(i) && defaults.hasOwnProperty(i)) {
                if (typeof defaults[i] === "object") {
                    overrideDefaults(newDefaults[i], defaults[i]);
                }
                else {
                    defaults[i] = newDefaults[i];
                }
            }
        }
    }

    modalLink.ajaxPostForm = null;
    modalLink.ajaxGetUrl = null;

    /**
     * Set new defaults for modalLink
     */
    modalLink.overrideDefaults = _global.overrideDefaults;

    /**
     * Open modal from target link element
     */
    modalLink.openLink = _global.openLink;

    /**
     * Open given url in modal
     */
    modalLink.open = _global.open;

    // deprecated
    modalLink.openUrl = _global.open;

    /**
     * Close all opened modal links
     */
    modalLink.close = _global.close;

    /**
     * Bind elements to to modalLink by selector
     */
    modalLink.bind = _global.bind;

    modalLink.onShowScroll = _global.onShowScroll;
    modalLink.onHideScroll = _global.onHideScroll;

    modalLink.onShowContentScroll = _global.onShowContentScroll;
    modalLink.onHideContentScroll = _global.onHideContentScroll;

    modalLink.stack = _global.stack;

    ns.modalLink = modalLink;

    var _helper = new (function () {

        var _this = this;
        var _htmlParserContainer = document.createElement("div");

        /**
         * Helper method for appending parameter to url
         */
        this.addUrlParam = function (url, name, value) {
            return _this.appendUrl(url, name + "=" + value);
        }

        /**
         * Hepler method for appending querystring to url
         */
        this.appendUrl = function (url, data) {
            return url + (url.indexOf("?") < 0 ? "?" : "&") + data;
        }

        /**
         * Returns first value of arguments which is not null or undefined
         */
        this.coalesce = function () {

            for (var i = 0; i < arguments.length; i++) {
                var arg = arguments[i];
                if (arg !== null && typeof (arg) !== "undefined") {
                    return arg;
                }
            }

            return null;
        }

        this.getAttrAsBoolean = function (el, attrName) {

            var attrValue = el.getAttribute(attrName);
            if (attrValue) {
                if (attrValue.toLowerCase() === "true" || attrValue === "1") {
                    return true;
                }
                if (attrValue.toLowerCase() === "false" || attrValue === "0") {
                    return false;
                }
            }
        }

        /**
         * 
         */
        this.getAttrAsInt = function (el, attrName) {

            var attrValue = el.getAttribute(attrName);
            if (!attrValue) {
                return undefined;
            }

            var res = parseInt(attrValue);
            if (isNaN(res)) {
                return undefined;
            }

            return res;
        }

        this.createElement = function (tagName, attr) {

            var el = document.createElement(tagName);

            for (var attrName in attr) {
                if (attr.hasOwnProperty(attrName)) {
                    el.setAttribute(attrName, attr[attrName]);
                }
            }

            return el;
        };

        this.parseHtml = function (html) {
            _htmlParserContainer.innerHTML = html;
            return _htmlParserContainer.children[0];
        };

        this.css = function (el, settings) {
            var style = el.style;
            for (var styleAttrName in settings) {
                if (settings.hasOwnProperty(styleAttrName)) {
                    style[styleAttrName] = settings[styleAttrName];
                }
            }
        };

        /**
         * Cleate deep clone of an object
         */
        this.clone = function (obj) {

            if (typeof (obj) === "undefined" || obj === null) {
                return obj;
            }

            var res = {};
            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    var val = obj[i];
                    if (typeof (val) === "object") {
                        val = _this.clone(val);
                    }
                    res[i] = val;
                }
            }

            return res;
        }

    })();

})(sparkling);